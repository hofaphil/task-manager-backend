# 💻 Task-Manager Backend

This project contains a simple backend for a task-manager for coding-interviews purposes only. The [task-manager-frontend](https://gitlab.com/hofaphil/task-manager-frontend) repository can be used as a frontend.

If your are doing a coding interview, check out the [conding-interview](link-tbd) branch.

## 🛠️ Project setup
The backend uses the Spring framework. To run this project you need to have a Java JDK and Maven installed.
Afterwards you can build the project:

```
./mvnw clean install
```

When you want to run the application simply use:
```
./mvnw spring-boot:run
```

## 📡 API
The backend provides the following API:

### DTO for communication
```java
public class TaskDTO {

    public Integer id;
    public Boolean completed;
    public String title;

}

```
### Endpoints
The following endpoints are provided:

---
##### Get all tasks
An endpoint which returns a list of all available tasks.
```
GET /tasks --(returns)--> TaskDTO[]
```
---
##### Get task
An endpoint which returns a list of all available tasks.
```
GET /tasks/{id} --(returns)--> TaskDTO
```
---
##### Update task
This endpoint need to insert or update a task. It then returns the TaskDTO representing the current data base state of the inserted/updated task.
```
POST /tasks --(returns)--> TaskDTO
```
---
##### Delete task
This endpoint deletes a task identified by the provided id.
```
DELETE /tasks/{id} --(returns)--> Void
```
