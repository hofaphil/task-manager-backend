package de.philipphofer.taskmanagerbackend.conversion;

import de.philipphofer.taskmanagerbackend.data.model.TaskModel;
import de.philipphofer.taskmanagerbackend.rest.model.TaskDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TaskDTOConverter implements Converter<TaskDTO, TaskModel> {

    @Override
    public TaskModel convert(TaskDTO dto) {
        TaskModel task = new TaskModel();

        task.id = dto.id;
        task.completed = dto.completed;
        task.title = dto.title;

        return task;
    }
}
