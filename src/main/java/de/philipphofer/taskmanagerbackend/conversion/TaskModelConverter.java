package de.philipphofer.taskmanagerbackend.conversion;

import de.philipphofer.taskmanagerbackend.data.model.TaskModel;
import de.philipphofer.taskmanagerbackend.rest.model.TaskDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TaskModelConverter implements Converter<TaskModel, TaskDTO> {

    @Override
    public TaskDTO convert(TaskModel model) {
        TaskDTO task = new TaskDTO();

        task.id = model.id;
        task.completed = model.completed;
        task.title = model.title;

        return task;
    }
}
