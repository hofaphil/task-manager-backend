package de.philipphofer.taskmanagerbackend.data;

import de.philipphofer.taskmanagerbackend.data.model.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<TaskModel, Integer> {
}
