package de.philipphofer.taskmanagerbackend.data.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class TaskModel {

    @Id
    @GeneratedValue
    public Integer id;

    public Boolean completed;

    public String title;
}
