package de.philipphofer.taskmanagerbackend.rest;

import de.philipphofer.taskmanagerbackend.rest.model.TaskDTO;
import de.philipphofer.taskmanagerbackend.service.TaskService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Validated
@RequestMapping("tasks")
@CrossOrigin(origins = {"http://127.0.0.1:3000", "http://localhost:3000"})
public class TaskEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(TaskEndpoint.class);
    private final TaskService taskService;

    public TaskEndpoint(@Autowired TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping()
    public TaskDTO updateTask(@RequestBody @Valid TaskDTO task) {
        logger.info("POST /tasks - with title {}", task.title);
        return taskService.updateTask(task);
    }

    @GetMapping()
    public List<TaskDTO> getAllTasks() {
        logger.info("GET /tasks");
        return taskService.getAllTasks();
    }

    @GetMapping("/{id}")
    public TaskDTO getTask(@PathVariable @NotNull Integer id) {
        logger.info("GET /tasks/{}", id);
        return taskService.getTask(id);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable @NotNull Integer id) {
        logger.info("DELETE /tasks/{}", id);
        taskService.deleteTask(id);
    }
}
