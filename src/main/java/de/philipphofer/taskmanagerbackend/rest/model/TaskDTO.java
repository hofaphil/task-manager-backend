package de.philipphofer.taskmanagerbackend.rest.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class TaskDTO {

    public Integer id;

    @NotNull
    public Boolean completed;

    @NotBlank
    public String title;
}
