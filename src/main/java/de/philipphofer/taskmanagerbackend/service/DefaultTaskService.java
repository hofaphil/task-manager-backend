package de.philipphofer.taskmanagerbackend.service;

import de.philipphofer.taskmanagerbackend.data.TaskRepository;
import de.philipphofer.taskmanagerbackend.data.model.TaskModel;
import de.philipphofer.taskmanagerbackend.rest.model.TaskDTO;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultTaskService implements TaskService {

    private final TaskRepository taskRepository;
    private final Converter<TaskDTO, TaskModel> dtoToModel;
    private final Converter<TaskModel, TaskDTO> modelToDTO;

    public DefaultTaskService(
            @Autowired TaskRepository taskRepository,
            @Autowired Converter<TaskDTO, TaskModel> dtoToModel,
            @Autowired Converter<TaskModel, TaskDTO> modelToDTO) {
        this.taskRepository = taskRepository;
        this.dtoToModel = dtoToModel;
        this.modelToDTO = modelToDTO;
    }

    @Override
    public TaskDTO updateTask(TaskDTO task) {
        TaskModel taskModel = dtoToModel.convert(task);
        if (taskModel == null) throw new EntityNotFoundException();
        return modelToDTO.convert(taskRepository.save(taskModel));
    }

    @Override
    public TaskDTO getTask(Integer id) {
        TaskModel task = taskRepository.getReferenceById(id);
        return modelToDTO.convert(task);
    }

    @Override
    public List<TaskDTO> getAllTasks() {
        List<TaskModel> tasks = taskRepository.findAll();
        return tasks.stream().map(modelToDTO::convert).toList();
    }

    @Override
    public void deleteTask(Integer id) {
        taskRepository.deleteById(id);
    }
}
