package de.philipphofer.taskmanagerbackend.service;

import de.philipphofer.taskmanagerbackend.rest.model.TaskDTO;

import java.util.List;

public interface TaskService {

    TaskDTO updateTask(TaskDTO task);

    TaskDTO getTask(Integer id);

    List<TaskDTO> getAllTasks();

    void deleteTask(Integer id);
}
