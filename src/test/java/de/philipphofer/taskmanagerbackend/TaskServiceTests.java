package de.philipphofer.taskmanagerbackend;

import de.philipphofer.taskmanagerbackend.data.TaskRepository;
import de.philipphofer.taskmanagerbackend.rest.model.TaskDTO;
import de.philipphofer.taskmanagerbackend.service.TaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = "spring.main.lazy-initialization=true")
@AutoConfigureTestDatabase
class TaskServiceTests {

    @Autowired
    private TaskService service;

    @Autowired
    private TaskRepository repository;

    // to be done...
}
